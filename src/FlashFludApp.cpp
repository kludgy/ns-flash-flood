#include "cinder/app/AppBasic.h"
#include "cinder/gl/gl.h"
#include "FluidSim.h"
#include "SimpleGUI.h"

using namespace ci;
using namespace ci::app;
using namespace std;
using namespace mowa::sgui;

class FlashFludAppApp : public AppBasic 
{
private:
	bool injecting;
	bool advecting;
	Vec2i prevpos;
	Rectf fluidtexbounds;

	SimpleGUI* gui;
	LabelControl* fpsLabel;

	float p_injectionDensity;
	float p_injectionSpeed;
	float p_viscosity;
	float p_diffusion;
	float p_bleedaway;
	int p_diffusionPasses;
	int p_projectionPasses;
	bool p_rendering;
	bool p_multithread;

public:
	FlashFludAppApp();
	void setup();
	void mouseDown( MouseEvent event );	
	void mouseUp( MouseEvent event );	
	void keyDown( KeyEvent event );
	bool clearFluid( MouseEvent event );
	void update();
	void draw();
};

FlashFludAppApp::FlashFludAppApp() :
	injecting(false),
	advecting(false),
	prevpos(0,0),
	gui(nullptr),
	fpsLabel(nullptr),
	p_injectionDensity(1000),
	p_injectionSpeed(30),
	p_viscosity(0.0001),
	p_diffusion(0.0001),
	p_bleedaway(0.003),
	p_diffusionPasses(20),
	p_projectionPasses(20),
	p_rendering(true),
	p_multithread(true)
{
}

void FlashFludAppApp::setup()
{
	gui = new SimpleGUI(this);
	fpsLabel = gui->addLabel("FPS");
	gui->addSeparator();
	gui->addParam("Injection Density", &p_injectionDensity, 0, 1000, 100);
	gui->addParam("Injection Speed", &p_injectionSpeed, 0, 1000, 30);
	gui->addParam("Viscosity", &p_viscosity, 0, 0.02, 0.001);
	gui->addParam("Diffusion", &p_diffusion, 0, 0.005, 0.001);
	gui->addParam("Bleedaway", &p_bleedaway, 0, 0.01, 0.003);
	gui->addParam("Diffusion Passes", &p_diffusionPasses, 1, 20, 20);
	gui->addParam("Projection Passes", &p_projectionPasses, 1, 20, 20);
	gui->addParam("Rendering", &p_rendering, true);
	gui->addParam("Multithreaded", &p_multithread, true);
	gui->addSeparator();
	gui->addButton("Clear")->registerClick(this, &FlashFludAppApp::clearFluid);

	ClearFluid();
}

void FlashFludAppApp::mouseDown( MouseEvent event )
{
	if (event.isLeft())
		injecting = true;
	
	if (event.isRight())
		advecting = true;
}

void FlashFludAppApp::mouseUp( MouseEvent event )
{
	if (event.isLeft())
		injecting = false;

	if (event.isRight())
		advecting = false;
}

void FlashFludAppApp::keyDown( KeyEvent event )
{
	if (event.getCode() == KeyEvent::KEY_DELETE)
		::ClearFluid();
}

bool FlashFludAppApp::clearFluid( MouseEvent event )
{
	::ClearFluid();
	return false;
}

void FlashFludAppApp::update()
{
	if (fluidtexbounds.getSize().lengthSquared() == 0)
		return;

	ClearFluidInjection(p_multithread);
	auto newpos = 
		Vec2i(
			(getMousePos().x - fluidtexbounds.getUpperLeft().x) * GetFluidGridWidth() / fluidtexbounds.getWidth(),
			(getMousePos().y - fluidtexbounds.getUpperLeft().y) * GetFluidGridHeight() / fluidtexbounds.getHeight()
		);

	if (injecting || advecting)
	{
		freal u = 0, v = 0, dens = 0;

		if (injecting)
			dens = p_injectionDensity;

		if (advecting)
		{
			auto delta = newpos - prevpos;
			u = delta.x * p_injectionSpeed;
			v = delta.y * p_injectionSpeed;
		}

		InjectFluidLine(prevpos.x, prevpos.y, newpos.x, newpos.y, u, v, dens);
	}

	prevpos = newpos;
	UpdateFluid(p_viscosity, p_diffusion, p_bleedaway, p_diffusionPasses, p_projectionPasses, 1/getFrameRate(), p_multithread ? FluidSimMode::FS_MULTITHREAD : FluidSimMode::FS_SINGLETHREAD);
}

double round(double d)
{
	return floor(d + 0.5);
}

void FlashFludAppApp::draw()
{
	gl::clear( Color( 0, 0, 0.2f ) ); 

	if (p_rendering)
	{
		const gl::Texture& tex = FlushFluidTexture();
		fluidtexbounds = Rectf(tex.getBounds()).getCenteredFit(getWindowBounds(), true);
		gl::disableAlphaBlending();
		gl::color(1,1,1);
		gl::draw(tex, fluidtexbounds);
	}

	stringstream fps;
	fps << (int)round(getAverageFps());
	fpsLabel->setText(fps.str());

	gui->draw();
}

CINDER_APP_BASIC( FlashFludAppApp, RendererGl )
