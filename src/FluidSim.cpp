#include "FluidSim.h"
#include "Resources.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Fbo.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Texture.h"
#include "cinder/Surface.h"
#include "cinder/app/App.h"
#include <ppl.h>

using namespace ci;
using namespace ci::app;
using namespace std;
using namespace Concurrency;

#define IX(i,j) ((i)+(N+2)*(j))
#define SWAP(x0,x) {freal *tmp=x0;x0=x;x=tmp;}

static const int N = 256;
static const int size = (N+2)*(N+2);
static freal u[size], v[size], u_prev[size], v_prev[size];
static freal dens[size], dens_prev[size];
static const int nthread = 8;
static const int mt_run1d = size/nthread;
static const Vec2i mt_run2d(N/nthread,N/nthread);
static vector<int> mt_runstarts1d(nthread);
static vector<Vec2i> mt_runstarts2d(nthread*nthread);

static gl::GlslProg* gpu_add_source;
static gl::GlslProg* gpu_advect;
static gl::GlslProg* gpu_diffuse;
static gl::GlslProg* gpu_project_div;
static gl::GlslProg* gpu_project_diffuse;
static gl::GlslProg* gpu_project_damp;
static gl::GlslProg* gpu_set_bnd;

static void InitRunstarts()
{
	for (int i = 0; i < nthread; ++i)
		mt_runstarts1d[i] = i*mt_run1d;

	for (int i = 0; i < nthread; ++i)
		for (int j = 0; j < nthread; ++j)
			mt_runstarts2d[j*nthread+i] = Vec2i(i*mt_run2d.x, j*mt_run2d.y);
}

static void InitGpuBits()
{
	gpu_add_source		= new gl::GlslProg( loadResource( RES_PASSTHRU_VERT ), loadResource( RES_FLUID_ADD_SOURCE_FRAG		) );
	gpu_advect			= new gl::GlslProg( loadResource( RES_PASSTHRU_VERT ), loadResource( RES_FLUID_ADVECT_FRAG			) );
	gpu_diffuse			= new gl::GlslProg( loadResource( RES_PASSTHRU_VERT ), loadResource( RES_FLUID_DIFFUSE_FRAG			) );
	gpu_project_div		= new gl::GlslProg( loadResource( RES_PASSTHRU_VERT ), loadResource( RES_FLUID_PROJECT_DIV_FRAG		) );
	gpu_project_diffuse	= new gl::GlslProg( loadResource( RES_PASSTHRU_VERT ), loadResource( RES_FLUID_PROJECT_DIFFUSE_FRAG ) );
	gpu_project_damp	= new gl::GlslProg( loadResource( RES_PASSTHRU_VERT ), loadResource( RES_FLUID_PROJECT_DAMP_FRAG	) );
	gpu_set_bnd			= new gl::GlslProg( loadResource( RES_PASSTHRU_VERT ), loadResource( RES_FLUID_SET_BND_FRAG			) );
}

static void set_bnd ( int N, int b, freal * x )
{
	int i;
	for ( i=1 ; i<=N ; i++ ) {
		x[IX(0  ,i)] = b==1 ? -x[IX(1,i)] : x[IX(1,i)];
		x[IX(N+1,i)] = b==1 ? -x[IX(N,i)] : x[IX(N,i)];
		x[IX(i,0  )] = b==2 ? -x[IX(i,1)] : x[IX(i,1)];x[IX(i,N+1)] = b==2 ? -x[IX(i,N)] : x[IX(i,N)];
	}
	x[IX(0  ,0  )] = 0.5*(x[IX(1,0  )]+x[IX(0  ,1)]);
	x[IX(0  ,N+1)] = 0.5*(x[IX(1,N+1)]+x[IX(0  ,N )]);
	x[IX(N+1,0  )] = 0.5*(x[IX(N,0  )]+x[IX(N+1,1)]);
	x[IX(N+1,N+1)] = 0.5*(x[IX(N,N+1)]+x[IX(N+1,N )]);
}

static void add_source ( int N, freal* x, const freal* s, freal dt )
{
	int i;
	for ( i=0 ; i<size ; i++ ) x[i] += dt*s[i];
}

static void add_source_gpu(gl::Fbo& x, const gl::Texture& s)
{
	gpu_add_source->bind();
	x.bindFramebuffer();
	gl::draw(s);
}

static void add_source_mt ( int N, freal* x, const freal* s, freal dt )
{
	parallel_for_each(mt_runstarts1d.begin(), mt_runstarts1d.end(),
		[=](int r0)
		{
			for (int j = r0; j < min(r0+mt_run1d, size); ++j)
				x[j] += dt*s[j];
		});
}

static void diffuse ( int N, int b, freal * x, const freal * x0, freal diff, int passes, freal dt )
{
	int k;
	freal a=dt*diff*N*N;
	for ( k=0 ; k<passes ; k++ ) 
	{
		for (int i=1 ; i<=N ; i++ ) {
			for (int j=1 ; j<=N ; j++ ) {
				x[IX(i,j)] = (x0[IX(i,j)] + a*(x[IX(i-1,j)]+x[IX(i+1,j)]+
					x[IX(i,j-1)]+x[IX(i,j+1)]))/(1+4*a);
			}
		}
		set_bnd ( N, b, x );
	}
}

static void diffuse_mt ( int N, int b, freal * x, const freal * x0, freal diff, int passes, freal dt )
{
	int k;
	freal a=dt*diff*N*N;
	for ( k=0 ; k<passes ; k++ ) 
	{
		parallel_for_each(mt_runstarts2d.begin(), mt_runstarts2d.end(),
			[=](const Vec2i& r0)
			{
				for (int i = r0.x+1; i < min(r0.x+1+mt_run2d.x, N+1); ++i)
				{
					for (int j = r0.y+1; j < min(r0.y+1+mt_run2d.y, N+1); ++j)
					{
						x[IX(i,j)] = (x0[IX(i,j)] + a*(x[IX(i-1,j)]+x[IX(i+1,j)]+
							x[IX(i,j-1)]+x[IX(i,j+1)]))/(1+4*a);
					}
				}
			});
		set_bnd ( N, b, x );
	}
}

static void advect ( int N, int b, freal * d, const freal * d0, const freal * u, const freal * v, freal dt )
{
	freal dt0 = dt*N;
	for (int i=1 ; i<=N ; i++ ) 
	{
		for (int j=1 ; j<=N ; j++ ) 
		{
			freal x = i-dt0*u[IX(i,j)]; 
			freal y = j-dt0*v[IX(i,j)];
			if (x<0.5) x=0.5; 
			if (x>N+0.5) x=N+ 0.5; 
			int i0=(int)x; 
			int i1=i0+1;
			if (y<0.5) y=0.5; 
			if (y>N+0.5) y=N+ 0.5; 
			int j0=(int)y; 
			int j1=j0+1;
			freal s1 = x-i0; 
			freal s0 = 1-s1; 
			freal t1 = y-j0; 
			freal t0 = 1-t1;
			d[IX(i,j)] = s0*(t0*d0[IX(i0,j0)]+t1*d0[IX(i0,j1)])+
				s1*(t0*d0[IX(i1,j0)]+t1*d0[IX(i1,j1)]);
		}
	}
	set_bnd ( N, b, d );
}

static void advect_mt ( int N, int b, freal * d, const freal * d0, const freal * u, const freal * v, freal dt )
{
	freal dt0 = dt*N;
	parallel_for_each(mt_runstarts2d.begin(), mt_runstarts2d.end(),
		[=](const Vec2i& r0)
		{
			for (int i = r0.x+1; i < min(r0.x+1+mt_run2d.x, N+1); ++i)
			{
				for (int j = r0.y+1; j < min(r0.y+1+mt_run2d.y, N+1); ++j)
				{
					freal x = i-dt0*u[IX(i,j)]; 
					freal y = j-dt0*v[IX(i,j)];
					if (x<0.5) x=0.5; 
					if (x>N+0.5) x=N+ 0.5; 
					int i0=(int)x; 
					int i1=i0+1;
					if (y<0.5) y=0.5; 
					if (y>N+0.5) y=N+ 0.5; 
					int j0=(int)y; 
					int j1=j0+1;
					freal s1 = x-i0; 
					freal s0 = 1-s1; 
					freal t1 = y-j0; 
					freal t0 = 1-t1;
					d[IX(i,j)] = s0*(t0*d0[IX(i0,j0)]+t1*d0[IX(i0,j1)])+
						s1*(t0*d0[IX(i1,j0)]+t1*d0[IX(i1,j1)]);
				}
			}
		});
	set_bnd ( N, b, d );
}

static void dens_step ( int N, freal * x, freal * x0,  freal * u, freal * v, freal diff, int diffpasses,
	freal dt )
{
	add_source ( N, x, x0, dt );
	SWAP ( x0, x ); diffuse ( N, 0, x, x0, diff, diffpasses, dt );
	SWAP ( x0, x ); advect ( N, 0, x, x0, u, v, dt );
}

static void dens_step_mt ( int N, freal * x, freal * x0,  freal * u, freal * v, freal diff, int diffpasses,
	freal dt )
{
	add_source_mt ( N, x, x0, dt );
	SWAP ( x0, x ); diffuse_mt ( N, 0, x, x0, diff, diffpasses, dt );
	SWAP ( x0, x ); advect_mt ( N, 0, x, x0, u, v, dt );
}

static void project ( int N, int passes, freal * u, freal * v, freal * p, freal * div )
{
	int k;
	freal h;
	h = 1.0/N;
	for ( int i=1 ; i<=N ; i++ ) {
		for ( int j=1 ; j<=N ; j++ ) {
			div[IX(i,j)] = -0.5*h*(u[IX(i+1,j)]-u[IX(i-1,j)]+
				v[IX(i,j+1)]-v[IX(i,j-1)]);
			p[IX(i,j)] = 0;
		}
	}
	set_bnd ( N, 0, div ); set_bnd ( N, 0, p );
	for ( k=0 ; k<passes ; k++ ) {
		for ( int i=1 ; i<=N ; i++ ) {
			for ( int j=1 ; j<=N ; j++ ) {
				p[IX(i,j)] = (div[IX(i,j)]+p[IX(i-1,j)]+p[IX(i+1,j)]+
					p[IX(i,j-1)]+p[IX(i,j+1)])/4;
			}
		}
		set_bnd ( N, 0, p );
	}
	for ( int i=1 ; i<=N ; i++ ) {
		for ( int j=1 ; j<=N ; j++ ) {
			u[IX(i,j)] -= 0.5*(p[IX(i+1,j)]-p[IX(i-1,j)])/h;
			v[IX(i,j)] -= 0.5*(p[IX(i,j+1)]-p[IX(i,j-1)])/h;
		}
	}
	set_bnd ( N, 1, u ); set_bnd ( N, 2, v );
}

static void project_mt ( int N, int passes, freal * u, freal * v, freal * p, freal * div )
{
	int k;
	freal h;
	h = 1.0/N;
	parallel_for_each(mt_runstarts2d.begin(), mt_runstarts2d.end(),
		[=](const Vec2i& r0)
		{
			for (int i = r0.x+1; i < min(r0.x+1+mt_run2d.x, N+1); ++i)
			{
				for (int j = r0.y+1; j < min(r0.y+1+mt_run2d.y, N+1); ++j)
				{
					div[IX(i,j)] = -0.5*h*(u[IX(i+1,j)]-u[IX(i-1,j)]+
						v[IX(i,j+1)]-v[IX(i,j-1)]);
					p[IX(i,j)] = 0;
				}
			}
		});
	set_bnd ( N, 0, div ); set_bnd ( N, 0, p );
	for ( k=0 ; k<passes ; k++ ) {
		parallel_for_each(mt_runstarts2d.begin(), mt_runstarts2d.end(),
			[=](const Vec2i& r0)
			{
				for (int i = r0.x+1; i < min(r0.x+1+mt_run2d.x, N+1); ++i)
				{
					for (int j = r0.y+1; j < min(r0.y+1+mt_run2d.y, N+1); ++j)
					{
						p[IX(i,j)] = (div[IX(i,j)]+p[IX(i-1,j)]+p[IX(i+1,j)]+
							p[IX(i,j-1)]+p[IX(i,j+1)])/4;
					}
				}
			});
		set_bnd ( N, 0, p );
	}
	parallel_for_each(mt_runstarts2d.begin(), mt_runstarts2d.end(),
		[=](const Vec2i& r0)
		{
			for (int i = r0.x+1; i < min(r0.x+1+mt_run2d.x, N+1); ++i)
			{
				for (int j = r0.y+1; j < min(r0.y+1+mt_run2d.y, N+1); ++j)
				{
					u[IX(i,j)] -= 0.5*(p[IX(i+1,j)]-p[IX(i-1,j)])/h;
					v[IX(i,j)] -= 0.5*(p[IX(i,j+1)]-p[IX(i,j-1)])/h;
				}
			}
		});
	set_bnd ( N, 1, u ); set_bnd ( N, 2, v );
}

static void vel_step ( int N, freal * u, freal * v, freal * u0, freal * v0,
	freal visc, int diffpasses, int projpasses, freal dt )
{
	add_source ( N, u, u0, dt ); add_source ( N, v, v0, dt );
	SWAP ( u0, u ); diffuse ( N, 1, u, u0, visc, diffpasses, dt );
	SWAP ( v0, v ); diffuse ( N, 2, v, v0, visc, diffpasses, dt );
	project ( N, projpasses, u, v, u0, v0 );
	SWAP ( u0, u ); SWAP ( v0, v );
	advect ( N, 1, u, u0, u0, v0, dt ); advect ( N,  2, v, v0, u0, v0, dt );
	project ( N, projpasses, u, v, u0, v0 );
}

static void vel_step_mt ( int N, freal * u, freal * v, freal * u0, freal * v0,
	freal visc, int diffpasses, int projpasses, freal dt )
{
	add_source_mt ( N, u, u0, dt ); add_source_mt ( N, v, v0, dt );
	SWAP ( u0, u ); diffuse_mt ( N, 1, u, u0, visc, diffpasses, dt );
	SWAP ( v0, v ); diffuse_mt ( N, 2, v, v0, visc, diffpasses, dt );
	project_mt ( N, projpasses, u, v, u0, v0 );
	SWAP ( u0, u ); SWAP ( v0, v );
	advect_mt ( N, 1, u, u0, u0, v0, dt ); advect_mt ( N,  2, v, v0, u0, v0, dt );
	project_mt ( N, projpasses, u, v, u0, v0 );
}

void ClearFluid()
{
	InitRunstarts();

	for (int i = 0; i < size; ++i)
	{
		u[i] = v[i] = u_prev[i] = v_prev[i] =
		dens[i] = dens_prev[i] = 0;
	}
}

void ClearFluidInjection(bool multithreaded)
{
	if (multithreaded)
	{
		parallel_for_each(mt_runstarts1d.begin(), mt_runstarts1d.end(),
			[=](int r0)
			{
				for (int i = r0; i < min(r0+mt_run1d, size); ++i)
					u_prev[i] = v_prev[i] = dens_prev[i] = 0;
			});
	}
	else
	{
		for (int i = 0; i < size; ++i)
		{
			u_prev[i] = v_prev[i] =
			dens_prev[i] = 0;
		}
	}
}

int GetFluidGridWidth()
{
	return N;
}

int GetFluidGridHeight()
{
	return N;
}

void InjectFluidPoint(int x, int y, freal u, freal v, freal dens)
{
	x++;
	y++;

	if (x >= N || x < 0 || y >= N || y < 0)
		return;

	u_prev[IX(x,y)] += u;
	v_prev[IX(x,y)] += v;
	dens_prev[IX(x,y)] += dens;
}

void InjectFluidLine(int x0, int y0, int x1, int y1, freal u, freal v, freal dens)
{
  int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
  int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1; 
  int err = (dx>dy ? dx : -dy)/2, e2;
 
  for(;;){
    InjectFluidPoint(x0, y0, u, v, dens);
    if (x0==x1 && y0==y1) break;
    e2 = err;
    if (e2 >-dx) { err -= dy; x0 += sx; }
    if (e2 < dy) { err += dx; y0 += sy; }
  }
}

void UpdateFluid(freal visc, freal diff, freal bleed, int diffpasses, int projpasses, freal dt, FluidSimMode mode)
{
	if (mode != FluidSimMode::FS_SINGLETHREAD)
	{
		parallel_for_each(mt_runstarts1d.begin(), mt_runstarts1d.end(),
			[=](int r0)
			{
				for (int j = r0; j < min(r0+mt_run1d, size); ++j)
					dens[j] *= 1 - bleed;
			});

		vel_step_mt ( N, u, v, u_prev, v_prev, visc, diffpasses, projpasses, dt );
		dens_step_mt ( N, dens, dens_prev, u, v, diff, diffpasses, dt );
	}
	else
	{
		for (int i = 0; i < size; ++i)
			dens[i] *= 1 - bleed;

		vel_step ( N, u, v, u_prev, v_prev, visc, diffpasses, projpasses, dt );
		dens_step ( N, dens, dens_prev, u, v, diff, diffpasses, dt );
	}
}

static cinder::Surface outsurf(N, N, true, SurfaceChannelOrder::UNSPECIFIED);
static gl::Texture* tex = nullptr;

const gl::Texture& FlushFluidTexture()
{
	Surface::Iter it = outsurf.getIter();

	int y=1;
	while (it.line())
	{
		int x=1;
		while (it.pixel())
		{
			int i = IX(x,y);

			it.r() = 0;
			it.g() = max(min((int)(dens[i] * 255), 255), 0);
			it.b() = 0;
			
			++x;
		}
		++y;
	}

	if (tex == nullptr)
		tex = new gl::Texture(N, N);

	tex->update(outsurf);
	return *tex;
}
