A simple Navier-Stokes advection sandbox that runs on Win32 and utilizes Microsoft's PPL to distribute the simulation. The utility depends on libcinder for it's application framework, and uses OpenGL for rendering. It would be useful to add GPU support in the future as well.
 (libcinder 0.8.3)
