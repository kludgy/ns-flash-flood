#pragma once
#include "cinder/CinderResources.h"

#define RES_GUI_FONT					CINDER_RESOURCE( ../resources/, pf_tempesta_seven.ttf, 128, TTF )

#define RES_PASSTHRU_VERT				CINDER_RESOURCE( ../shaders/		, passThru_vert.glsl	, 129, GLSL )
#define RES_FLUID_ADD_SOURCE_FRAG		CINDER_RESOURCE( ../shaders/fluid/	, add_source_frag.glsl	, 130, GLSL )
#define RES_FLUID_ADVECT_FRAG			CINDER_RESOURCE( ../shaders/fluid/	, advect_frag.glsl		, 131, GLSL )
#define RES_FLUID_DIFFUSE_FRAG			CINDER_RESOURCE( ../shaders/fluid/	, diffuse_frag.glsl		, 132, GLSL )
#define RES_FLUID_PROJECT_DIV_FRAG		CINDER_RESOURCE( ../shaders/fluid/	, project_div.glsl		, 133, GLSL )
#define RES_FLUID_PROJECT_DIFFUSE_FRAG 	CINDER_RESOURCE( ../shaders/fluid/	, project_diffuse.glsl	, 134, GLSL )
#define RES_FLUID_PROJECT_DAMP_FRAG		CINDER_RESOURCE( ../shaders/fluid/	, project_damp.glsl		, 135, GLSL )
#define RES_FLUID_SET_BND_FRAG			CINDER_RESOURCE( ../shaders/fluid/	, set_bnd_frag.glsl		, 136, GLSL )
