#pragma once

#include "cinder/gl/gl.h"

typedef float freal;

enum FluidSimMode
{
	FS_SINGLETHREAD,
	FS_MULTITHREAD,
	FS_GPU,
};

void ClearFluid();
int GetFluidGridWidth();
int GetFluidGridHeight();
void ClearFluidInjection(bool multithreaded);
void InjectFluidPoint(int x, int y, freal u, freal v, freal dens);
void InjectFluidLine(int x0, int y0, int x1, int y1, freal u, freal v, freal dens);
void UpdateFluid(freal viscosity, freal diffusion, freal bleedAway, int diffusionPasses, int projectionPasses, freal timeElapsed, FluidSimMode mode);
const cinder::gl::Texture& FlushFluidTexture();
